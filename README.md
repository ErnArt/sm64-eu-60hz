# Super Mario 64 EU 60Hz

- This project is a fork of the original sm64 decompilation, as a challenge I'm trying to give the old 50Hz rom a 60Hz treatment ! It's very experimental, here the game is running faster than the original rom, but a simple change of audio refresh rate makes the sound clean. The video mode was changed in the main.c and the rom region was changed in the rom_headers.s, you need to compile with these arguments to avoid a checksum verification and compiling an european rom :
```bash
make VERSION=eu -j4 COMPARE=0
```

For more informations of the original repo, go here : `https://github.com/n64decomp/sm64.git`
Big thanks to red from the official discord server for helping me !